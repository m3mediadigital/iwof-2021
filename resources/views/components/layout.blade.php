<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-9W34RZWVE1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-9W34RZWVE1');
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description"
		content="A iWof é uma empresa de tecnologia com engajamento social na geração de oportunidades remuneradas.">
	<meta name="keywords"
		content="app, aplicativo, andoid, ios, tecnlogoia, emprego, dinheiro, remuneração, grana, renda extra, oportunidades, vagas, trablho">
	<meta property=”og:image” content=”https://www.iwof.com.br/images/favicon-310.png">
	<link rel="shortcut icon" href="images/favicon1.png" type="image/png">

    <title>iWof - Trabalhe onde quiser, como quiser</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link href="{{ asset('argon') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    {{ @$css }}

    {{-- <script src="{{ asset('js/custom_element.js') }}"></script> --}}
</head>
<body class="bg-white">
    {{-- <custom-policy></custom-policy> --}}
    {{ $content }}
    <script src="{{ mix('js/app.js') }}">
    </script>
    {{ @$js }}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        @if(session('success'))
            Swal.fire(
                'Sucesso',
                '{!! session('success') !!}',
                'success'

            )
        @endif

        @if(session('error'))
            Swal.fire(
                'Erro!',
                '{!! session('error') !!}',
                'error',
            )
        @endif
    </script>
</body>
</html>
