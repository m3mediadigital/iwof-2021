@component('mail::message')
# Nova mensagem




<p><strong>Nome:</strong> {{ $items['name'] }}</p>
<p><strong>Telefone:</strong> {{ $items['phone'] }}</p>
<p><strong>E-mail:</strong> {{ $items['email'] }}</p>
<p><strong>Messagem:</strong> {{ $items['message'] }}</p>



Obrigado,<br>
{{ config('app.name') }}
@endcomponent
