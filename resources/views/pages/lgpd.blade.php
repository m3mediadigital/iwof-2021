{{-- <!doctype html>
<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LGPD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
  </head>
  <body> --}}
<x-layout>
    <x-slot name='content'>
        <div class="lgpd">
            <section class="container pt-5">
                <div class="pb-4">
                    <h1>Política de privacidade</h1>
                </div>
                <div>
                    <p>
                        Nós da iWof Tecnologia Ltda temos um compromisso com você e, por isso,
                        buscamos toda segurança e privacidade com os seus dados pessoais
                        cadastrados. Além disso, nossa Política de privacidade tem o objetivo
                        de assegurar e garantir toda transparência necessária para o uso e
                        aproveitamento seguro do nosso produto.
                    </p>
                </div>
                <div>
                    <p>
                        O nosso principal intuito é sanar as suas dificuldades e, pensando nisso,
                        deixamos a nossa Política de Privacidade mais acessível e clara possível.
                        Porém, caso ainda haja dúvidas, nós temos canais de atendimento direcionados
                        exclusivamente para você.  Os nossos canais oficiais de suporte estarão
                        disponíveis para você em nosso site e aplicativo destinados a tirar todas as suas dúvidas.
                    </p>
                </div>
                <div>
                    <p>
                        Diante disso, nossa política de privacidade expõe e detalha informações
                        muito importantes sobre o tratamento dos seus dados, quais coletamos,
                        como realizamos essa ação a para qual finalidade. Fazemos isso para que
                        fique claro para você os seus direitos, no que se refere a esses dados, sugerimos atenção.
                    </p>
                </div>
            </section>
            <section class="container pt-4">
                <div class="pt-2">
                    <h2>
                        Introdução
                    <h2>
                </div>
                <div>
                    <p>
                        Ao utilizar o sistema da iWof, seus dados pessoais, os quais são concedidos
                        por você, são informados para que possamos proporcionar a utilização dos
                        nossos serviços e garantir que você tenha uma excelente experiência junto
                        conosco. Aqui na iWof não há espaço para insegurança e estamos sempre
                        desenvolvendo a melhor maneira de assegurar e preservar a completude dos seus dados pessoais.
                    </p>
                </div>
                <div>
                    <p>
                        É importante lembrar que, aceitando nossa Política de privacidade, você torna-se
                        conscientemente um conhecedor de que a iWof Tecnologia Ltda será a responsável
                        pelo armazenamento e tratamento dos seus dados cadastrados.
                    </p>
                </div>
                <div>
                    <p>
                        A iWof está inscrita no CNPJ sob nº 37.335.552/0001-60, sediada na cidade de Natal,
                        Estado do Rio Grande do Norte, na Rua General Felizardo Brito, 2936 – Capim Macio.
                        – TBridge – Natal/RN – CEP: 59078-410
                    </p>
                </div>
                <div>
                    <p>
                        Ainda assim, caso algo ainda não tenha ficado claro, nós estaremos à disposição
                        para detalhar ainda mais qualquer informação e sanar todas as suas dúvidas,
                        de modo a garantir que você possa se sentir seguro.
                    </p>
                </div>
            </section>
            <section class="container">
                <div>
                    <h2>
                        Aplicação
                    </h2>
                </div>
                <div>
                    <p>
                        Toda nossa Política de Privacidade é adequada e admissível para todos os usuários da iWof.
                    </p>
                </div>
                <div>
                    <p>
                        Assim, seus dados pessoais são coletados em todas as ocasiões de uso do aplicativo em seu
                        aparelho celular; utilização dos nossos serviços oferecidos, a fim de ampliar o
                        desenvolvimento de inovações e tecnologias destinados a você; quando você navega
                        em nosso website (https://iwof.com.br); ao manter contato com nossos canais oficiais
                        de suporte e ao contribuir com nossas pesquisas e campanhas.
                    </p>
                </div>
                <div>
                    <p>
                        A Política de privacidade da iWof é direcionada e atinge especificamente os
                        usuários e clientes da iWof, ou seja,  todas as pessoas que, de fato,
                        utilizam e usufruem dos nossos serviços, sejam elas pessoas físicas ou
                        jurídicas, como também, pessoas naturais captadas pela iWof que não tenha,
                        efetivamente, se tornado um usuário.
                    </p>
                </div>
                <div>
                    <p>
                        Nessa Política de Privacidade você encontrará detalhadamente todas as
                        maneiras de recolhimento dos seus dados pessoais cadastrados, assim como
                        de uso dos mesmos, sendo aplicada a todos os meios de coleta, com o objetivo
                        de desenvolvimento e aperfeiçoamento das nossas funcionalidades.
                    </p>
                </div>
                <div>
                    <p>
                        Sob esse viés, destaca-se a Lei nº 13.709/2018 (Lei Geral de Proteção de Dados),
                        a qual fundamenta toda a construção dessa Política de Privacidade, aplicada ao
                        tratamento dos seus dados pessoais no território Brasileiro, estando submetidas
                        às leis locais, assim como à mencionada anteriormente.
                    </p>
                </div>
            </section>
            <section class="container">
                <div>
                    <h2>
                        Dados pessoais coletados pela iWof
                    </h2>
                </div>
                <div>
                    <p>
                        Ao fornecer seus dados pessoais, na contratação dos serviços da iWof, nós realizamos
                        a coleta dos dados relacionados à você. A coleta é feita quando há tal
                        compartilhamento, assim como de forma automática, desde o primeiro momento
                        de interação com a plataforma e através dos nossos canais oficiais de atendimento e suporte.
                    </p>
                </div>
                <div>
                    <p>
                        Assim, aceitando os termos postos aqui nessa Política de Privacidade, você demonstrará
                        concordância expressa no fornecimento de dados pessoais confiáveis e atualizados sobre
                        você, além da não modificação dos dados que pertencem à sua titularidade, durante a
                        utilização e contratação dos serviços da iWof. Por tanto, você será o único responsável
                        pelas informações inverídicas fornecidas diretamente à iWof.
                    </p>
                </div>
            </section>
            <section class="container">
                <div>
                    <p>
                        Trazemos, a seguir, os dados pessoais tratados pela iWof:
                    </p>
                </div>
                <div>
                    <h4>
                        Dados concedidos pelo titular
                    </h4>
                </div>
                <ul>
                    <li>
                        Dados cadastrais, como: nome completo, data de nascimento,
                        documentos de identificação, nome da empresa, razão social,
                        endereço, gênero e outros.
                    </li>
                    <li>
                        Dados de contato, como telefone e e-mail
                    </li>
                    <li>
                        Grau de escolaridade e experiências e cursos profissionalizantes
                    </li>
                    <li>
                        Dados biométricos, tais como a fotografia do seu documento de identificação e do seu perfil.
                    </li>
                </ul>
                <div>
                    <h4>
                        Dados públicos
                    </h4>
                </div>
                <div>
                    <p>
                        Alguns dados sobre você são coletados de forma pública, ou seja, foram dispostos
                        publicamente por você através de interações, menções ou depoimentos sobre a iWof
                        em suas redes sociais, ou páginas na web, assim como vinculadas à sua foto e imagem.
                    </p>
                </div>
                <div>
                    <h4>
                        Dados retirados do dispositivo:
                    </h4>
                </div>
                <ul>
                    <li>
                        Endereço IP do dispositivo móvel utilizado para acessar os serviços ou produtos da iWof
                    </li>
                    <li>
                        Geolocalização do seu dispositivo a partir da sua autorização para direcionamento assertivo de vagas.
                    </li>
                    <li>
                        Cookies
                    </li>
                    <li>
                        Alguns atributos do dispositivo, tais como ID, sistema operacional, navegador e modelo.
                    </li>
                    <li>
                        Dados técnicos, como informações de URL, de conexão de rede, do provedor, e do dispositivo
                    </li>
                </ul>
                <div>
                    <h4>
                        Dados pessoais gerados do uso da iWof
                    </h4>
                </div>
                <ul>
                    <li>
                        Dados de transações e movimentações financeiras,incluindo também informações dos remetentes e beneficiários.
                    </li>
                    <li>
                        Todo histórico de atendimento ao cliente
                    </li>
                </ul>
            </section>
            <section class="container">
                <div>
                    <h2>
                        Como seus dados pessoais são utilizados pela iWof
                    </h2>
                </div>
                <div>
                    <p>
                        A utilização dos seus dados pessoais possuem sempre uma finalidade e objetivos
                        claros, buscando sempre propor alta qualidade em nossas entregas e uma experiência
                        satisfatória. Pensando em tudo isso, segue abaixo, para qual propósito utilizamos seus dados:
                    </p>
                </div>
                <div>
                    <p>
                    Dados pessoais informados pelo titular
                    </p>
                </div>
                <div>
                    <p>
                        Objetivos:
                    </p>
                </div>
                <div>
                    <p>
                        Atendimento e suporte para dúvidas e solicitações feitas
                    </p>
                </div>
                <div>
                    <p>
                        Realização de contato pelos meios de comunicação: telefone, SMS, e-mail, envio de push notifications, whatsapp dentre outros meios.
                    </p>
                </div>
                <div>
                    <p>
                        Possibilitar a otimização de envio de oportunidades ligadas às empresas parceiras e você.
                    </p>
                </div>
                <div>
                    <p>
                        Envio de presentes ou cartas como lembrança do contato existente entre a iWof e você.
                    </p>
                </div>
                <div>
                    <p>
                        Oferecimento dos serviços e produtos da iWof
                    </p>
                </div>
                <div>
                    <p>
                        Aperfeiçoamento de uma entrega voltada às suas necessidades e das empresas parceiras, destinando o cruzamento de informações,
                        também, para esse intuito, oferecendo sempre melhores produtos e serviços.
                    </p>
                </div>
                <div>
                    <p>
                        Identificação e verificação de requisitos para contratação dos serviços e produtos da iWof.
                    </p>
                </div>
                <div>
                    <p>
                        Revisões regulares e recorrentes acerca de possíveis problemas técnicos e de segurança, visando a prevenção e resolução dos mesmos.
                    </p>
                </div>
                <div>
                    <p>
                        Captações, pesquisas de satisfação e de mercado, marketing, envio de informações sobre novas atualizações ou novidades em nossos
                        serviços, assim como notícias, conteúdos e eventos criados exclusivamente para você.
                    </p>
                </div>
                <div>
                    <p>
                        Quando necessário, para o exercício regular de direitos da iWof, na apresentação de documentos em processos judiciais ou administrativos.
                    </p>
                </div>
                <div>
                    <p>
                        Preservar sempre e manter uma posição colaborativa com órgãos fiscalizadores, autoridades competentes ou cumprimento de ordem judicial.
                    </p>
                </div>
                <div>
                    <p>
                        Cumprimento de obrigação legal ou regulatória.
                    </p>
                </div>
                <div>
                    <p>
                        Dados biométricos
                    </p>
                </div>
                <div>
                    <p>
                        Objetivos:
                    </p>
                </div>
                <div>
                    <p>
                        Preservação da segurança e assertividade nos processos de reconhecimento e legitimação dos cadastros Prevenção, prevenindo fraude.
                    </p>
                </div>
                <div>
                    <p>
                        Dados pessoais que coletamos de terceiros
                    </p>
                </div>
                <div>
                    <p>
                        Objetivos:
                    </p>
                </div>
                <div>
                    <p>
                        Aperfeiçoamento das nossas entregas de produtos e serviços iWof, visando atender todas as suas necessidades.
                    </p>
                </div>
                <div>
                    <p>
                        Pesquisas de satisfação pessoal e de mercado, marketing, captações e ações de melhoria para a sua experiência.
                    </p>
                </div>
                <div>
                    <p>
                        Em caso de emergência com um profissional da iWof, facilitaria a resolução de dificuldades.
                    </p>
                </div>
                <div>
                    <p>
                        Garantir sempre a segurança dos nossos clientes da iWof, prevenindo fraudes, atitudes ilícitas
                        e possibilitando possíveis investigações, quando necessário.
                    </p>
                </div>
                <div>
                    <p>
                        Execução e efetivação de obrigações legais ou regulatórias impostas a iWof, na manutenção
                        do cadastro, incluindo normas necessárias à prevenção de movimentações financeiras ilícitas e dentre outros.
                    </p>
                </div>
                <div>
                    <p>
                        Dados coletados do dispositivo:
                    </p>
                </div>
                <div>
                    <p>
                        Objetivos:
                    </p>
                </div>
                <div>
                    <p>
                        Desenvolver e gerar relatórios de estatísticas, estudos e pesquisas com
                        melhorias na prestação de serviços e aprimoramento do uso e da experiência gerada com a aplicação e site da iWof.
                    </p>
                </div>
                <div>
                    <p>
                        Indicação e avisos sobre novos serviços, produtos ou funcionalidades do aplicativo, assim como serviços de parceiros que possam te interessar.
                    </p>
                </div>
                <div>
                    <p>
                        Liberação de publicidade, seja no nosso site ou redes sociais.
                    </p>
                </div>
                <div>
                    <p>
                        Produção de conteúdo, pesquisas, estudos e levantamentos acerca do uso do aplicativo e de todos os serviços oferecidos.
                    </p>
                </div>
                <div>
                    <p>
                        Revisões regulares e recorrentes acerca de possíveis problemas técnicos e de segurança, visando a prevenção e resolução dos mesmos.
                    </p>
                </div>
                <div>
                    <p>
                        Dados de geolocalização para sua segurança e assertividade no direcionamento de oportunidades e serviços.
                    </p>
                </div>
                <div>
                    <p>
                        Garantir sempre a segurança dos nossos clientes da iWof, prevenindo fraudes, atitudes ilícitas e
                        possibilitando possíveis investigações, quando necessário.
                    </p>
                </div>
                <div>
                    <p>
                        Execução regular de direitos da iWof, apresentando, quando necessário, documentos em processos judiciais e administrativos.
                    </p>
                </div>
                <div>
                    <p>
                        Executar ordem judicial, de autoridade competente ou de órgão fiscalizador
                    </p>
                </div>
                <div>
                    <p>
                        Cumprimento de obrigação legal ou regulatória.
                    </p>
                </div>
                <div>
                    <h4>
                        Dados pessoais originados do uso da iWof
                    </h4>
                </div>
                <div>
                    <p>
                        Objetivos:
                    </p>
                </div>
                <div>
                    Aperfeiçoamento das nossas entregas de produtos e serviços iWof.
                </div>
                <div>
                    <p>
                        Atendimento e suporte destinados a você, para dúvidas e solicitações feitas.
                    </p>
                </div>
                <div>
                    <p>
                        Aperfeiçoamento de uma entrega voltada às suas necessidades e das empresas parceiras, destinando o cruzamento de informações,
                        também, para esse intuito, oferecendo sempre melhores produtos e serviços.
                    </p>
                </div>
                <div>
                    <p>
                        Evolução no desenvolvimento de novos produtos e serviços a serem oferecidos pela iWof.
                    </p>
                </div>
                <div>
                    <p>
                        Cumprimento de obrigação legal ou regulatória.
                    </p>
                </div>
                <div>
                    <p>
                        Desenvolvimento do nosso ambiente de testes, a fim de proporcionar melhorias na aplicação que possam satisfazer o cliente iWof.
                    </p>
                </div>
                <div>
                    <p>
                        Captações, pesquisas de satisfação e de mercado, marketing, envio de informações sobre novas atualizações ou novidades em nossos
                        serviços, assim como notícias, conteúdos e eventos criados exclusivamente para você.
                    </p>
                </div>
                <div>
                    <p>
                        Envio de presentes ou cartas como lembrança do contato existente entre a iWof e você
                    </p>
                </div>
                <div>
                    <p>
                        Revisões regulares e recorrentes acerca de possíveis problemas técnicos e de segurança, visando a prevenção e resolução dos mesmos.
                    </p>
                </div>
                <div>
                    <p>
                        Garantir sempre a segurança dos nossos clientes da iWof, prevenindo fraudes,
                        atitudes ilícitas e possibilitando possíveis investigações, quando necessário.
                    </p>
                </div>
                <div>
                    <p>
                        Quando necessário, para o exercício regular de direitos da iWof, na apresentação de documentos em processos judiciais ou administrativos
                    </p>
                </div>
                <div>
                    <p>
                        Preservar sempre e manter uma posição colaborativa com órgãos fiscalizadores, autoridades competentes ou cumprimento de ordem judicial.
                    </p>
                </div>
                <div>
                    <p>
                        Dados públicos
                    </p>
                </div>
                <div>
                    <p>
                        Objetivos:
                    </p>
                </div>
                <div>
                    <p>
                        Envio de presentes ou cartas como lembrança do contato existente entre a iWof e você.
                    </p>
                </div>
                <div>
                    <p>
                        Execução e efetivação de obrigações legais ou regulatórias necessárias a iWof, na manutenção do cadastro, incluindo normas indispensáveis
                        à prevenção de movimentações financeiras ilícitas e dentre outros.
                    </p>
                </div>
                <div>
                    <p>
                        Exposição dos produtos e serviços oferecidos pela iWof, desenvolvendo uma comunicação
                        voltada para a divulgação dos mesmos nos meios publicitários e digitais.
                    </p>
                </div>
                <div>
                    <p>
                        Garantir sempre a segurança dos nossos clientes da iWof, prevenindo fraudes,
                        atitudes ilícitas e possibilitando possíveis investigações, quando necessário
                    </p>
                </div>
                <div>
                    <p>
                        Quando necessário, para o exercício regular de direitos da iWof, na apresentação de documentos em processos judiciais ou administrativos
                    </p>
                </div>
                <div>
                    <p>
                        Seus dados poderão ser compartilhados, caso você solicite. Esse compartilhamento terá uma
                        destinação objetiva e poderá ser realizado com provedores e fornecedores de serviços
                        importantes para o desenvolvimento das nossas atividades, assim como para autoridades
                        e órgãos judiciais, com o objetivo legal regulador. Mas, não se preocupe!
                        Todo compartilhamento realizado será feito de acordo com toda previsão legal aplicável.
                    </p>
                </div>
                <div>
                    <p>
                        Diante disso, gostaríamos de compartilhar com você quais as esferas de fornecedores com quem normalmente compartilhamos seus dados:
                    </p>
                </div>
                <div>
                    <p>
                        Parceiros como as instituições de ensino profissionalizante parceiras da iWof,
                        empresas que recebem o trabalhador iWof para prestação de serviço, empresas que
                        oferecem serviços de tecnologia da informação, de serviços estatísticos e
                        marketing, assim como serviços financeiros para viabilizar os meios de pagamento.
                    </p>
                </div>
                <div>
                    <p>
                        Objetivos do compartilhamento:
                    </p>
                </div>
                <div>
                    <p>
                        Realização de contato pelos meios de comunicação: telefone, SMS, e-mail, envio de push notifications, whatsapp dentre outros meios.
                    </p>
                </div>
                <div>
                    <p>
                        Assegurar a segurança necessária para as empresas parceiras da iWof que receberão os trabalhadores para prestação do serviço.
                    </p>
                </div>
                <div>
                    <p>
                        Executar ordem judicial, de autoridade competente ou de órgão fiscalizador.
                    </p>
                </div>
                <div>
                    <p>
                        Melhorias na prestação de serviços e aprimoramento do uso e da experiência gerada com a aplicação e site da iWof.
                    </p>
                </div>
                <div>
                    <p>
                        Contribuição para o desenvolvimento de melhorias do nosso produto e serviços ofertados.
                    </p>
                </div>
                <div>
                    <p>
                        Avaliações acerca dos dados cadastrados, com fins de validação acerca da sua
                        identidade e reavaliações periódicas para identificar possíveis necessidades de prospecção.
                    </p>
                </div>
                <div>
                    <p>
                        Quando necessário, para o exercício regular de direitos da iWof, na apresentação de documentos em processos judiciais ou administrativos.
                    </p>
                </div>
                <div>
                    <p>
                        Captações, pesquisas de satisfação e de mercado, marketing, envio de informações sobre novas atualizações ou novidades
                        em nossos serviços, assim como notícias, conteúdos e eventos criados exclusivamente para você.
                    </p>
                </div>
                <div>
                    <p>
                        Revisões regulares e recorrentes acerca de possíveis problemas técnicos e de segurança, visando a prevenção e resolução dos mesmos.
                    </p>
                </div>
                <div>
                    <p>
                        Garantir sempre a segurança dos nossos clientes da iWof,
                        prevenindo fraudes, atitudes ilícitas e possibilitando possíveis investigações, quando necessário.
                    </p>
                </div>
                <div>
                    <p>
                        Execução e efetivação de obrigações legais ou regulatórias necessárias a iWof.
                    </p>
                </div>
                <div>
                    <p>
                        Objetivos do compartilhamento:
                    </p>
                </div>
                <div>
                    <p>
                        Garantir sempre a segurança dos nossos clientes da iWof, prevenindo fraudes, atitudes ilícitas e possibilitando possíveis investigações, quando necessário
                    </p>
                </div>
                <div>
                    <p>
                        Quando necessário, para o exercício regular de direitos da iWof, na apresentação de documentos em processos judiciais ou administrativos
                    </p>
                </div>
                <div>
                    <p>
                        Executar ordem judicial, de autoridade competente ou de órgão fiscalizado.
                    </p>
                </div>
                <div>
                    <p>
                        Preservar sempre e manter uma posição colaborativa com órgãos fiscalizadores, autoridades competentes ou cumprimento de ordem judicial.
                    </p>
                </div>
                <div>
                    <p>
                        De acordo com a sua solicitação:
                    </p>
                </div>
                <div>
                    <p>
                        Objetivos do compartilhamento:
                    </p>
                </div>
                <div>
                    <p>
                        Nós da iWof prezamos sempre pela transparência e clareza para
                        que você possa se sentir seguro junto conosco, ao compartilhar informações preciosas sobre você.
                    </p>
                </div>
                <div>
                    <p>
                        Envio de mensagens e notificações informativas por e-mails, push notifications, WhatsApp e SMS.
                    </p>
                </div>
                <div>
                    <p>
                        Permanência e exclusão dos seus dados cadastrados.
                    </p>
                </div>
                <div>
                    <p>
                        Todos os dados fornecidos por você serão armazenados de forma segura, e mantidos em
                        um ambiente controlado, durante todo o período em que você estiver junto conosco utilizando nossos serviços.
                    </p>
                </div>
                <div>
                    <p>
                        Mesmo após o cancelamento de sua conta, nós poderemos armazenar seus dados cadastrados
                        durante um período adicional, de modo a cumprir nossas obrigações legais e regulatórias
                        que foram descritas anteriormente. Mas não se preocupe! Durante esse período os seus
                        dados poderão ser consultados por nós para cumprir com as obrigações e preservar nossos direitos em caso de ações judiciais.
                    </p>
                </div>
                <div>
                    <p>
                        Seus direitos como titular dos dados pessoais.
                    </p>
                </div>
                <div>
                    <p>
                        Como titular dos dados pessoais informados, há direitos concedidos à você frente aos controladores dos mesmos,
                        a partir da entrada em vigor da LGPD. Pensando nisso, nossa Política de privacidade entende a necessidade de
                        que você possa entender quais os direitos lhe são cabíveis e, assim, irá detalhar, a seguir, como você poderá exercê-los:
                    </p>
                </div>
                <div>
                    <p>
                        Existência de tratamento de dados pessoais
                    </p>
                </div>
                <div>
                    <p>
                        Como informamos anteriormente, seus dados pessoais cadastrados são armazenados em ambiente seguro
                        e controlável pela iWof durante todo o período em que você estiver junto conosco utilizando nossos
                        serviços. Assim, ao se tornar nosso cliente, seus dados já estarão sendo tratados, mesmo que seja
                        nesse armazenamento seguro e poderá, também, solicitar à iWof que confirme se realiza o tratamento dos seus dados pessoais.
                    </p>
                </div>
                <div>
                    <p>
                        Conhecimento e acesso aos dados pessoais.
                    </p>
                </div>
                <div>
                    <p>
                        A informação acerca dos dados pessoais que a iWof possui sobre você, poderão ser solicitados sempre que desejar.
                    </p>
                </div>
                <div>
                    <p>
                        Alteração de dados já cadastrados para correção ou atualização.
                    </p>
                </div>
                <div>
                    <p>
                        Em caso de dados não cadastrados, cadastrados incorretamente ou incompletos, você poderá solicitar correção
                        dos mesmos junto a iWof. Caso o dado que você deseja acrescentar, atualizar ou corrigir não possa ser
                        feito diretamente por você em seu APP, você poderá solicitar alteração pela nossa equipe, sempre
                        comprovando a veracidade do dado desejado de acordo com o documento correspondente.
                    </p>
                </div>
                <div>
                    <p>
                        Exclusão e bloqueio de dados desnecessários ou excessivos em desconformidade com a LGPD.
                    </p>
                </div>
                <div>
                    <p>
                        Caso você identifique que algum dos seus dados cadastrados estão sendo tratados de
                        forma excessiva ou para finalidade diversa a qual foi destinado, estando em desconformidade
                        com a LGPD, você poderá solicitar que a iWof realize a exclusão ou bloqueio desses
                        dados, desde que esteja comprovado tal desconformidade com a disposição legal.
                    </p>
                </div>
                <div>
                    <p>
                        Eliminação dos dados pessoais após consentimento.
                    </p>
                </div>
                <div>
                    <p>
                        Caso fique constatado que seus dados estão sendo tratados e utilizados de forma
                        desnecessária, ou seja, que não estejam claros os objetivos e destinação dos
                        mesmos ou que estão em desconformidade com a LGPD, você poderá solicitar a eliminação desses dados.
                    </p>
                </div>
                <div>
                    <p>
                        Quais empresas a iWof recebeu ou forneceu dados referente à você para determinado fim.
                    </p>
                </div>
                <div>
                    <p>
                        Há sim a possibilidade que você solicite à iWof quais foram os terceiros que foram compartilhados seus dados ou de quem recebeu.
                    </p>
                </div>
                <div>
                    <p>
                        Suspensão do consentimento
                    </p>
                </div>
                <div>
                    <p>
                        A suspensão do seu consentimento para o tratamento dos seus dados pessoais é possível.
                        Caso isso aconteça, pode ocasionar a suspensão direta de algumas funcionalidades do
                        aplicativo ou até mesmo o fim dos serviços prestados. De toda maneira, isso não impedirá
                        o uso de dados anonimizados ou cujo tratamento seja pautado em uma outra possibilidade legal da LGPD.
                    </p>
                </div>
                <div>
                    <p>
                        Informação sobre a possibilidade e consequências de não fornecimento do consentimento.
                    </p>
                </div>
                <div>
                    <p>
                        Em situações que seu consentimento seja necessário para acessar algum produto
                        ou serviço na iWof, você poderá questionar quais as consequências do não
                        consentimento e se há possibilidade de fornecimento sem o consentimento prévio.
                    </p>
                </div>
                <div>
                    <p>
                        Decisões automatizadas
                    </p>
                </div>
                <div>
                    <p>
                        Garantimos que toda nossa aplicação é pautada nas previsões legais e princípios
                        éticos, mas você poderá solicitar, quando desejar, a revisão de decisões tomadas
                        de acordo com o tratamento automatizado de dados pessoais e quais os parâmetros
                        que levam tais decisões. Diante disso, por questões de confidencialidade de negócio
                        e preservação da concorrência, nós não informamos ou fornecemos detalhes acerca do funcionamento dos sistemas automatizados.
                    </p>
                </div>
                <div>
                    <p>
                        Portabilidade e direito de petição
                    </p>
                </div>
                <div>
                    <p>
                        Você poderá solicitar à iWof a portabilidade dos seus dados pessoais a outro prestador
                        de serviços, quando regulamentado tal direito pela Autoridade Nacional de Proteção de
                        Dados (ANPD). Ademais, com a ANPD, você poderá peticionar em relação aos seus dados diante a autoridade nacional.
                    </p>
                </div>
                <div>
                    <p>
                        Se você desejar exercer tais direitos, você poderá entrar em contato conosco por meio
                        do nosso canal oficial de atendimento que é o “suporte”, através do nosso site
                        www.iwof.com.br ou através do seu próprio aplicativo. A validação da sua identidade poderá ser feita de forma a prevenir fraudes
                    </p>
                </div>
                <div>
                    <p>
                        Controle e registro de atividades.
                    </p>
                </div>
                <div>
                    <p>
                        Suas movimentações e ações em nosso site ou aplicativo podem ficar registradas a partir
                        da criação de logs que irão conter o endereço  IP, a ação realizada, data e horário,
                        detalhes sobre o dispositivo utilizado e geolocalização. Além disso, nós também podemos
                        utilizar tecnologia própria ou de terceiro que tem o objetivo de realizar o monitoramento das ações feitas por você.
                    </p>
                </div>
                <div>
                    <p>
                        Cookies: São pequenos arquivos criados por sites visitados e que são salvos no computador
                        do usuário, por meio do navegador de forma a armazenar de forma temporária o que você está
                        visitando na rede. Os cookies podem ser utilizados para diversas finalidades como lembrá-lo
                        de suas preferências, recolher informações que possam ser utilizadas para fornecer conteúdos
                        e materiais de forma mais adequada ou até mesmo manter registrado as informações já pesquisadas
                        por você. Diante disso, destacamos que a iWof dispõe de cookies em seu website.
                    </p>
                </div>
                <div>
                    <p>
                        Gostaríamos de ressaltar e lembrá-lo que você poderá bloquear, a qualquer momento, o uso
                        de cookies em seu navegador ativando uma configuração destinada para isso. Dessa forma,
                        como a limitação e bloqueio do uso de cookies está sujeita a uma configuração do seu
                        navegador, estará condicionada a limitações do mesmo. Além disso, você também poderá bloqueá-los da mesma maneira.
                    </p>
                </div>
                <div>
                    <p>
                        Transferência internacional
                    </p>
                </div>
                <div>
                    <p>
                        O armazenamento dos seus dados pessoais poderão ser feitos em servidores de computação
                        em nuvens localizadas fora do Brasil, assim, alguns deles ou até mesmo todos poderão ser
                        transferidos para o exterior dessa maneira. Mas não se preocupe! A iWof se preocupa com
                        todas as disposições legais vigentes e realiza a prática de ações seguras, preservando
                        sua privacidade e a confidencialidade dos seus dados.
                    </p>
                </div>
                <div>
                    <p>
                        Além disso, a iWof aplica medidas e ações que prezam sempre pela sua segurança,
                        comprometendo-se com a integridade dos seus dados na coleta e armazenamento dos
                        mesmos. Seus dados podem ser armazenados por meios de tecnologia de cloud computing
                        e outras tecnologias que surjam futuramente, pensando sempre no melhor para você
                        e sempre respeitando nossa política de privacidade. Possuímos uma equipe muito
                        competente e responsável para garantir que seus dados sejam tratados com toda a
                        segurança necessária utilizando as melhores práticas.
                    </p>
                </div>
                <div>
                    <p>
                        Múltiplo fator de autenticação para acesso às informações;
                    </p>
                </div>
                <div>
                    <p>
                        Criptografia para dados em repouso, em trânsito e em uso, para garantir a integridade das informações;
                    </p>
                </div>
                <div>
                    <p>
                        Monitoramento contínuo do ambiente;
                    </p>
                </div>
                <div>
                    <p>
                        Sugerimos que, por segurança, não forneça dados de acesso ao aplicativo com ninguém, sua senha é apenas de seu interesse e domínio.
                    </p>
                </div>
                <div>
                    <p>
                        Auditorias periódicas.
                    </p>
                </div>
                <div>
                    <p>
                        Segurança como código, com o objetivo de desenvolver automações e respostas rápidas e eficientes para eventos de segurança no ambiente tecnológico;
                    </p>
                </div>
                <div>
                    <p>
                        Análises e testes contínuos de segurança da informação em nossos sistemas, realizado por nosso time.
                    </p>
                </div>
                <div>
                    <p>
                        Sobre o seu consentimento
                    </p>
                </div>
                <div>
                    <p>
                        É importante ressaltar e relembrar que alguns dados só serão coletados pela
                        iWof com o seu consentimento e só poderão ser tratados após autorização
                        prévia de acordo com as descrições das finalidades desejadas. Além disso, o
                        tratamento dos seus dados pessoais é uma situação muito importante para a
                        iWof e nos preocupamos sempre para que você tenha uma experiência segura e
                        transparente junto conosco.O tratamento dos seus dados pessoais pela iWof
                        é requisito indispensável para que  possamos entregar nossos produtos e serviços ofertados, da melhor maneira possível.
                    </p>
                </div>
                <div>
                    <p>
                        Ao término da sua leitura, você poderá aceitar todos os termos e condições
                        expostas aqui, consentindo com o tratamento dos seus dados pessoais de acordo
                        com as maneiras aqui expostas e detalhadas. Diante disso, mesmo diante das
                        informações aqui colocadas, em caso de dúvidas, nós estaremos sempre à
                        disposição para falar com você, bastante entrar em contato com nossos canais
                        de suporte para falar com um dos nossos atendentes.
                    </p>
                </div>
                <div>
                    <p>
                        Atualizações e mudanças  nesta Política de Privacidade
                    </p>
                </div>
                <div>
                    <p>
                        Essa Política de Privacidade poderá sofrer mudanças ou atualizações ao longo
                        do tempo e, em casos como esse, você será notificado para que possa conferi-lá
                        novamente de forma integral e poderá aceitá-la novamente de acordo com as
                        alterações realizadas, se caso a mudança estiver na forma como tratamos seus
                        dados pessoais ou a finalidade para qual estamos tratando-os e que dependam
                        do seu consentimento. De todas as formas, a Política de Privacidade da Iwof
                        sempre seguirá assiduamente todas as exigências legais impostas.
                    </p>
                </div>
                <div>
                    <p>
                        Portanto, sempre que alguma alteração for feita, você será notificado e um
                        comunicado será enviado para você de modo que você possa se manter ciente
                        que algo mudou. Caso você permaneça utilizando nossos serviços, você estará
                        concordando com as novas condições, mas se houver alguma discordância da sua
                        parte, você sempre poderá nos informar através do suporte.
                    </p>
                </div>
                <div>
                    <p>
                        Caso, após a leitura da nossa Política de Privacidade, ainda houver dúvidas
                        acerca de algum ponto abordado ou caso você precise falar conosco, você poderá
                        entrar em contato através do nosso casal oficial de atendimento que é o
                        “suporte”, o qual você poderá acessar através do nosso site www.iwof.com.br
                        ou através do seu próprio aplicativo na aba de “suporte”.
                    </p>
                </div>
            </section>
        </div>
    </x-slot>
</x-layout>
    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script> --}}

