<x-layout>
    <x-slot name="content">
        <main class="index d-flex align-items-center justify-content-center flex-column">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6 index-profissional d-flex align-items-center justify-content-center flex-column py-3">
                        <a href="{{ route('site') }}" class="text-decoration-none text-white fw-bold fs-3 d-flex align-items-center justify-content-center flex-column">
                            <p class="text-center pb-5 pb-lg-3">A procura de <strong>oportunidade de trabalho</strong>?</p>
                            <img src="{{ asset('images/logo_index.svg') }}" class="mt-lg-3" alt="{{ env('APP_NAME') }}">
                        </a>                        
                    </div>
                    <div class="col-12 d-md-none p-0 m-0 d-flex align-items-center justify-content-center flex-column">
                        <hr>
                    </div>
                    <div class="col-12 col-lg-6 index-empresa d-flex align-items-center justify-content-center flex-column py-3">
                        <a href="{{ route('empresa') }}" class="text-decoration-none text-white fw-bold fs-3 d-flex align-items-center justify-content-center flex-column">
                            <p class="text-center pb-5 pb-lg-3">A procura de <strong>profissionais e soluções para o seu negócio</strong>?</p>
                            <img src="{{ asset('images/logo_empresa.svg') }}" class="mt-lg-3" alt="{{ env('APP_NAME') }}">
                        </a>
                    </div>
                </div>
            </div>
            <div class="index-footer position-fixed w-100">
                <img src="{{ asset('images/cicle.png') }}" class="index-footer" alt="{{ env('APP_NAME') }}">
            </div>            
        </main>
    </x-slot>
</x-layout>