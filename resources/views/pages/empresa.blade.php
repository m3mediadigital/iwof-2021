<x-layout>
    <x-slot name="content">
        <header>
            <img src="{{ asset('images/cicle.png') }}" class="position-absolute header" alt="{{ env('APP_NAME') }}">
            <div class="container">
                <div class="row pt-5">
                    <div class="col-12 d-flex justify-content-center pt-3 pb-5">
                        <img src="{{ asset('images/logo_empresa.svg') }}" class="logo" alt="{{ env('APP_NAME') }}">
                    </div>
                    <div class="col-12 col-md-8 col-lg-6 col-xl-5 pt-4 pb-md-5 pt-lg-5">
                        <h1 class="text-white fw-bolder pt-5">Conectamos para transformar</h1>
                        <p class="pt-2 text-white lh-lg fw-light pb-5">Conectamos a meta das empresas ao sonho de pessoas, facilitando o encontro entre trabalhadores autônomos e empresas parceiras transformamos realidades</p>
                        <div class="d-flex justify-content-center justify-content-md-start pb-md-3">
                            <a href="" class="btn rounded-pill d-flex align-items-center">
                                <img src="{{ asset('images/icon_more.svg') }}" class="ps-1 pe-3" alt="{{ env('APP_NAME') }}"> Conheça-nos
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer position-absolute w-100 d-flex">
                <img src="{{ asset('images/cicle_seconde.png') }}" class="footer-img" alt="{{ env('APP_NAME') }}">
            </div>
        </header>
        <section class="about">
            <img src="{{ asset('images/cicle_blue.svg') }}" class="header position-absolute" alt="{{ env('APP_URL') }}">
            <div class="container py-5">
                <div class="row py-5">
                    <div class="col-12 col-md-5 col-lg-6 d-md-flex align-items-md-center flex-column justify-content-md-center align-items-lg-start">
                        <h1 class="fw-bolder pb-4">Temos a solução</h1>
                        <p class="fw-light lh-lg pb-4">Conectamos a meta das empresas aos sonhos de pessoas, facilitando o encontro entre trabalhadores e empresas transformando muitas realidades</p>
                    </div>
                    <div class="col-12 col-md-7 col-lg-6">
                        <img src="{{ asset('images/img-about.png') }}" class="img-fluid" alt="{{ env('APP_NAME') }}">
                    </div>
                </div>
            </div>
        </section>

        <section class="numbers pt-lg-5">
            <div class="container pb-lg-5">
                <h1 class="fw-bolder pb-4">Nossos números</h1>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3 py-3">
                        <div class="card border-0 d-flex align-items-center bg-transparent">
                            <div class="card-body rounded-circle text-center d-flex justify-content-center flex-column">
                                <h1 class="card-title text-white fw-bolder m-0">+ de 55.000</h1>
                                <p class="card-text text-white">Usuários</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-3">
                        <div class="card border-0 d-flex align-items-center bg-transparent">
                            <div class="card-body nth-child rounded-circle text-center d-flex justify-content-center flex-column">
                                <h1 class="card-title text-white fw-bolder m-0">+ de 130.000</h1>
                                <p class="card-text text-white">Oportunidades criadas</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-3">
                        <div class="card border-0 d-flex align-items-center bg-transparent">
                            <div class="card-body rounded-circle text-center d-flex justify-content-center flex-column">
                                <h1 class="card-title text-white fw-bolder m-0">+ de 600.000</h1>
                                <p class="card-text text-white">Horas Trabalhadas</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 py-3">
                        <div class="card border-0 d-flex align-items-center bg-transparent">
                            <div class="card-body nth-child rounded-circle text-center d-flex justify-content-center flex-column">
                                <p class="card-text text-white">Clientes de diversos segmentos</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer w-100 d-flex justify-content-end">
                <img src="{{ asset('images/cicle_cinza.svg') }}" class="footer-img" alt="{{ env('APP_NAME') }}">
            </div>
        </section>
        <footer class="contact w-100">
            <div class="bg py-5 position-absolute"></div>
            <div class="container pt-5">
                <div class="row pt-3 pb-lg-4 pt-xl-5">
                    <div class="col-12 col-md-5 col-lg-4 pb-lg-3 pt-lg-5">
                        <h1 class="fw-bolder text-white">Vamos conversar?</h1>
                        <x-form action="{{ route('contato') }}">
                            @csrf
                            <div class="row pb-5">
                                <div class="form-group pt-3">
                                    <div class="form-floating">
                                        <x-input name="name" class="form-control bg-transparent border-0 border-bottom border-white rounded-0 text-white" id="Nome" placeholder="Nome" required />
                                        <x-label class="text-white" for="Nome" />
                                    </div>
                                    <x-error field="name" class="text-danger" />
                                </div>
                                <div class="form-group pt-3">
                                    <div class="form-floating">
                                        <x-input name="phone" type="text" class="form-control sp_celphones bg-transparent border-0 border-bottom border-white rounded-0 text-white" id="Telefone" placeholder="Telefone" required />
                                        <x-label class="text-white" for="Telefone" />
                                    </div>
                                    <x-error field="phone" class="text-danger" />
                                </div>
                                <div class="form-group pt-3">
                                    <div class="form-floating">
                                        <x-input name="email" type="email" class="form-control bg-transparent border-0 border-bottom border-white rounded-0 text-white" id="E-mail" placeholder="E-mail" required />
                                        <x-label class="text-white" for="E-mail" />
                                    </div>
                                    <x-error field="email" class="text-danger" />
                                </div>
                                <div class="form-group pt-3">
                                    <div class="form-floating">
                                        <x-textarea name="message" class="form-control bg-transparent border-0 border-bottom border-white rounded-0 text-white" id="Messagem" placeholder="Messagem" required />
                                        <x-label class="text-white" for="Messagem" />
                                    </div>
                                    <x-error field="message" class="text-danger" />
                                </div>
                                <div class="col-12 d-flex justify-content-center pt-5">
                                    <button type="submit" class="btn rounded-pill d-flex align-items-center">
                                        <img src="{{ asset('images/icon_more.svg') }}" class="ps-1 pe-3" alt="{{ env('APP_NAME') }}"> Enviar
                                    </button>
                                </div>
                            </div>
                        </x-form>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 pt-5">
                        <img src="{{ asset('images/img-contact.png') }}" class="img-fluid mt-md-4 mt-lg-0" alt="{{ env('APP_NAME') }}">
                    </div>
                </div>
            </div>
            <div class="footer w-100 pt-xl-1">
                <img src="{{ asset('images/cicle_cinza.svg') }}" class="footer-img" alt="{{ env('APP_NAME') }}">
            </div>
        </footer>
    </x-slot>
    <x-slot name="js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

            $('.sp_celphones').mask(SPMaskBehavior, spOptions);
        </script>
    </x-slot>
</x-layout>
