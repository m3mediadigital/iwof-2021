<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use App\Models\Contacts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller
{
    public function index()
    {
        return view('pages.index');
    }

    public function empresa()
    {
        return view('pages.empresa');
    }

    public function site()
    {
        return view('pages.site');
    }

    public function paraiba()
    {
        return view('pages.paraiba');
    }

    public function contato(Request $request)
    {

        try {
            Contacts::create($request->all());
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('error','Por favor tente novamente');

        } finally {
            Mail::to('contato@iwof.com.br')->send(new Contact($request->all()));

        }

        return back()->with('success','Em breve entraremos em contato.');
    }

    public function lgpd()
    {
        return view('pages.lgpd');
    }
}
